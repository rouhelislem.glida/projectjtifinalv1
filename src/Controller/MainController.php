<?php


namespace App\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use JMS\Serializer\SerializerBuilder;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class MainController extends AbstractController
{

    public $em;
    public $passwordEncoder;
    public function __construct( UserPasswordEncoderInterface $passwordEncoder, EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @param $object
     * @return Response
     */
    public function successResponse($object)
    {
        $serializer = SerializerBuilder::create()->build();
        $data = $serializer->serialize($object, 'json');
        $response = new Response($data);
        $response->headers->set('Content-type','application/json');
        return $response;
    }

}
